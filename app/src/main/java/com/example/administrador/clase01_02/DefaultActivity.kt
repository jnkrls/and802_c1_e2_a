package com.example.administrador.clase01_02

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class DefaultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default)
    }
}
